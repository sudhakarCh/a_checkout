package com.ex.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CheckoutExGitlabApplication {

	public static void main(String[] args) {
		SpringApplication.run(CheckoutExGitlabApplication.class, args);
	}

}
